package io.github.grooters.idles.controller;

import io.github.grooters.idles.bean.User;
import io.github.grooters.idles.utils.Printer;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Random;

public class AccountC {

    private long time = 0;

    private boolean end = false;

    public String createToken(int length){

        String str="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

        String time = String.valueOf(System.currentTimeMillis());

        Random random=new Random();

        StringBuilder stringBuilder=new StringBuilder(time);

        for(int i=0; i< length; i++){

            int number=random.nextInt(str.length());

            stringBuilder.append(str.charAt(number));

        }

        Printer.print("token", stringBuilder.toString());

        return stringBuilder.toString();
    }

    public String createVerification(int length){

        String str="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

        Random random=new Random();

        StringBuilder stringBuilder=new StringBuilder();

        for(int i=0; i< length; i++){

            int number=random.nextInt(str.length());

            stringBuilder.append(str.charAt(number));

        }

        Printer.print("verification", stringBuilder.toString());

        return stringBuilder.toString();
    }

    public String createRandomNumber(){

        String str="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

        String time = String.valueOf(System.currentTimeMillis());

        Random random=new Random();

        StringBuilder stringBuilder=new StringBuilder(time);

//        for(int i=0; i < 10; i++){
//
//            int number=random.nextInt(str.length());
//
//            stringBuilder.append(str.charAt(number));
//
//        }

        Printer.print("随机账号生成", "账号：" + stringBuilder.toString());

        return stringBuilder.toString();
    }

    public long getNetworkTime() {

        try {

            URL url=new URL("http://www.baidu.com");

            URLConnection conn=url.openConnection();

            conn.connect();

            return conn.getDate();

        }catch (IOException e) {
            e.printStackTrace();
        }

        return 0;
    }

    public void startTiming(){
        new Timer().start();
    }

    public long getTime(){

        return time;

    }

    public void stopTiming(){

        end = true;

        time = 0;

    }

    class Timer extends Thread{

        @Override

        public void run() {

            super.run();

            while (!end){

                try {

                    Thread.sleep(1000 );

                    time = time + 1;

                    Printer.print("统计时长", "time：" + time);

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

        }
    }
}
