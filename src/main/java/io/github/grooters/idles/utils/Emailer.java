package io.github.grooters.idles.utils;

import io.github.grooters.idles.base.Code;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;

public class Emailer {

    private HtmlEmail htmlEmail;

    public final String HOST_NAME_163 = "smtp.163.com";
    public final String HOST_NAME_QQ = "smtp.QQ.com";

    private String sendEmail;

    public Emailer(String sendEmail, String number, String password){

        htmlEmail = new HtmlEmail();

        this.sendEmail = sendEmail;

        htmlEmail.setHostName("smtp." + sendEmail.split("@")[1]);

//        htmlEmail.setSmtpPort(25);

        htmlEmail.setAuthentication(number, password);

        htmlEmail.setStartTLSEnabled(false);

        htmlEmail.setSSLOnConnect(false);

    }

    public int sendEmail(String title, String content, String...receiveEmail){

        try {

            htmlEmail.setFrom(sendEmail);

            htmlEmail.addTo(receiveEmail);

            htmlEmail.setCharset("UTF-8");

            htmlEmail.setSubject(title);

            htmlEmail.setHtmlMsg(content);

            Printer.print("email", "发送邮箱：" + sendEmail, "接收邮箱：" + receiveEmail[0]);

            htmlEmail.send();

            return Code.REGISTER_SUCCESS_VERIFICATION;

        } catch (EmailException e) {

            e.printStackTrace();

            return Code.REGISTER_FAILURE_VERIFICATION;

        }

    }

}
